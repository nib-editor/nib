#include "history.h"
#include <stdlib.h>

struct _history_node {
    struct _history_node* prev;
    enum _operation op;
    piece* pce;
};

struct _history {
    struct _history_node* head;
    struct _history_node* curr;
};

struct _history* history_init(void)
{
    struct _history* new_hist = malloc(sizeof(struct _history));
    if (new_hist == NULL) {
        new_hist->head = NULL;
        new_hist->curr = NULL;
    }
    return new_hist;
}

void history_cancel_redos(struct _history* hist)
{
    // TODO
}

struct _history_node* history_push(struct _history* hist, enum _operation op,
    piece* pce)
{
    history_cancel_redos(hist);
    /* TODO actually add the thing */
    /* TODO fix the free/insertion history/piecetab dilemma */
    return NULL; /* !!! */
}

/* TODO history_revise */
