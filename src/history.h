#ifndef __NIB_HISTORY_H

#define __NIB_HISTORY_H
#define HISTORY_OK 0
#define HISTORY_ERR 1

typedef struct _piece piece;
typedef struct _history history;
typedef struct _history_node history_node;

enum _operation {
    INS,
    DEL
};

history* history_init(void);
struct _history_node* history_push(struct _history* hist, enum _operation op, piece* pce);

#endif
