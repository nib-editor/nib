/* vim: set et ai ts=4 sw=4 tw=80:
 *
 * Copyright George White, Rasmus Jepsen (c) 2018.
 * All rights reserved.
 *
 * Distributed under the MIT License.
 * See file LICENSE in the root of this source code.
 */

#include "nib.h"
#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>

struct arguments args = {
    .no_color = 0, /* --no-color: disable coloured stdout */
    .daemon = 0, /* --daemon: */
    .background = 0, /* --background: fork in background
                                               (--daemon only)
                                               vice-versa: --foreground */
    .socket = "127.0.0.1:64283" /* --socket <socket>: use <socket> for communication */
};

/* Forward definitions of non-exported functions */
editor editor_init(struct arguments* args);
editor daemon_editor_init(struct arguments* args);
void editor_mainloop(editor* ed);
int editor_close(editor* ed);

/* Arguments
 *
 * We follow the GNU argument specification, found here:
 * https://www.gnu.org/prep/standards/html_node/Option-Table.html#Option-Table
 *
 * Please ensure there is not already a defined long name and (if necessary)
 * short flag, before using your own, otherwise, use the one specified.
 */
static struct option long_options[] = {
    { "no-color", no_argument, &args.no_color, 1 },
    { "daemon", no_argument, &args.daemon, 1 },
    { "background", no_argument, &args.background, 1 },
    { "foreground", no_argument, &args.background, 0 },
    { "socket", required_argument, 0, 's' },
    { 0, 0, 0, 0 }
};

int main(int argc, char* argv[])
{
    int opt;
    int index;
    while ((opt = getopt_long(argc, argv, "", long_options, &index)) != -1) {
        switch (opt) {
        case 0:
            /* Long option.
				 *
				 * Use long_option[index] to determine what it 
                                 * is.
				 */
            break;
        default:
            abort();
        }
    }

    editor ed;
#ifndef NIB_DAEMON_ONLY
    if (!args.daemon) {
        ed = editor_init(&args);
    } else {
#endif /* NIB_DAEMON_ONLY */
        ed = daemon_editor_init(&args);
#ifndef NIB_DAEMON_ONLY
    }
#endif /* NIB_DAEMON_ONLY */

    editor_mainloop(&ed);
    return editor_close(&ed);
}

editor
editor_init(struct arguments* args)
{
    initscr(); /* prepare ncurses */

    editor ed = {
        .retcode = EXIT_SUCCESS
    };

    return ed;
}

editor
daemon_editor_init(struct arguments* args)
{
    editor ed = {
        .retcode = EXIT_SUCCESS
    };

    return ed;
}

void editor_mainloop(editor* ed)
{
    message msg;
    call c;

    while (msg.type != MESSAGE_EXIT) {
        msg = ed->get_next_message();
        while (c != NULL; c = msg->get_next_call()) {
        }

        ed->refresh_co();
    }
}

int editor_close(editor* ed)
{
    endwin(); /* de-initialise ncurses */
    return EXIT_SUCCESS;
}
