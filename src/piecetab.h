#ifndef __NIB_PIECETAB_H
#define __NIB_PIECETAB_H

#include "history.h"
#include <stdio.h>

#define PIECETAB_OK 0
#define PIECETAB_ERR 1

typedef struct _piecetab piecetab;

piecetab* piecetab_init(FILE*);

void piecetab_free(piecetab*);

struct _piece* piecetab_insert(piecetab*,
    char*,
    long,
    size_t,
    piece*,
    history*);

#endif // __NIB_PIECETAB_H
