/* vim: set et ai ts=4 sw=4 tw=80: */
#ifndef NIB_DAEMON_ONLY
#include <ncurses.h>
#endif

struct arguments {
    int no_color;
    int daemon;
    int background;
    char* socket[255];
};

typedef struct _editor {
    int retcode;
} editor;

typedef enum _message_type {
    MESSAGE_EXIT,
} message_type;

typedef struct _message {
    message_type type;
} message;

typedef struct _call {

} call;
