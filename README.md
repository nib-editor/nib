# nib

Just a text editor, honest.

## Building

nib uses CMake, purely because of its vast integrated (albeit complex) ecosystem
which permits greater user flexibility without the expense of developer time.

CMake is a build system _generator_. It doesn't build anything by itself, though
it can use its 'partner' build system programs with `cmake --build`.

It is imperative that you build nib out of source. Most people would set it up
in a new folder at the root of the clone, named `build`, thus:

	mkdir build
	cd build
	cmake --build .. -G 'Ninja'

Where 'Ninja' may be optionally replaced with another generator of your choice,
such as 'Unix Makefiles'.
